# PROMISE.methods

The goal of PROMISE.methods is to provide functions for creating the data
dictionary and other functions for the dataset and method documentation. This is
a companion package to the main PROMISE package.

## Installation

You can install PROMISE.methods from GitLab with:

```r
# install.packages("pak")
pak::pak("gitlab::promise-cohort/PROMISE.methods")
```

## Usage

This package and it's functions are used in combination with the main PROMISE 
package to add to the data dictionary. It also contains the methods for the 
PROMISE data collection. Please see the associated PROMISE documentation for 
more details about usage.

You can view the methods or dictionary by running:

```r
view_manual('methods')
view_manual('dictionary')
```
